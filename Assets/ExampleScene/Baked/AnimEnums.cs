public enum Model : short { Horse, LardAssSpider, Kitten }
public static class ModelSize { public const short Size = 3; }

public enum Horse : byte { Horse_Walk, Horse_Idle, Horse_Run }
public static class HorseSize { public const byte Size = 3; }
public enum LardAssSpider : byte { Attack, Death, Idle, Run }
public static class LardAssSpiderSize { public const byte Size = 4; }
